# About

Small command line tool to control media players that implement [MediaPlayer2.Player dbus specification](http://specifications.freedesktop.org/mpris-spec/latest/Player_Interface.html).

You can modify/reupload the script in any way you want.